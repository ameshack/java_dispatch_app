package com.sendyit.com.sendyutils;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Random;
import java.util.regex.Pattern;

public class ValidationClass {

    public static boolean isInteger(String input) {
        try {
            Integer.parseInt(input);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public static int getInteger(String input) {
        try {
            return Integer.parseInt(input);

        } catch (Exception ex) {
            return 0;
        }
    }

    public static boolean isDouble(String input) {
        try {
            Double.parseDouble(input);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public static double getDouble(String input) {
        try {
            return Double.parseDouble(input);

        } catch (Exception ex) {
            return 0;
        }
    }

    public static boolean ValidEmail(String EMAIL_ADDRESS) {
        Pattern rfc2822 = Pattern.compile("^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$");
        if (rfc2822.matcher(EMAIL_ADDRESS).matches()) {
            // Well formed email
            return true;
        }
        return false;

    }


    public static String randomString(int len) {
        Random rnd = new Random();
        String AB = "123456789ABCDEFGHIJKLMNPQRSTUVWXYZ";
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++) {
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        }


        return sb.toString();

    }
    public static String randomText(int len) {
        Random rnd = new Random();
        String AB = "123456789ABCDEFGHIJKLMNPQRSTUVWXYZ";
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++) {
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        }


        return sb.toString();

    }

    public static String randomNumber(int len) {
        Random rnd = new Random();
        String AB = "0123456789";
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++) {
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        }


        return sb.toString();

    }

    public static String randomStringAZ(int len) {
        Random rnd = new Random();
        String AB = "ABCDEFGHIJKLMNPQRSTUVWXYZ";
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++) {
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        }


        return sb.toString();

    }


    public static boolean isJSONValid(String test) {
        if(test != null) {
            try {
                new JSONObject(test);
                return true;
            } catch (JSONException ex) {
                return false;
            }
        }
        return false;
    }

    public static String getInternationalPhone(String PassedphoneNumber, String PassedCountry) {
        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();

        String NewphoneNumber = "1";

        try {
            PhoneNumber phoneNumberFormated = phoneUtil.parse(PassedphoneNumber, PassedCountry);

            if (phoneUtil.isValidNumber(phoneNumberFormated)) {
                NewphoneNumber = phoneUtil.format(phoneNumberFormated, PhoneNumberFormat.E164);

            }

        } catch (NumberParseException e) {
            System.out.println("NumberParseException was thrown: " + e.toString());

        }

        return NewphoneNumber;
    }


    public static String getPhone(String PassedphoneNumber, String PassedCountry, PhoneNumberFormat phoneFormat) {
        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();

        String NewphoneNumber = "";

        try {
            PhoneNumber phoneNumberFormated = phoneUtil.parse(PassedphoneNumber, PassedCountry);

            if (phoneUtil.isValidNumber(phoneNumberFormated)) {
                NewphoneNumber = phoneUtil.format(phoneNumberFormated, phoneFormat);

            }

        } catch (NumberParseException e) {
            System.out.println("NumberParseException was thrown: " + e.toString());

        }

        return NewphoneNumber;
    }

}
