package com.sendyit.com.mqttclient;
import com.sendyit.com.redis.PartnerPositions;
import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
/**
 * Created by alloysmeshack on 17/06/2016.
 */
public class SendyMqttClient {

    private org.eclipse.paho.client.mqttv3.MqttClient client;

    private static final String TOPIC_PARTNER_APP_POSITIONS = "partner_app_positions";
    private static final String TOPIC_CUSTOMER_POSITIONS = "customer_positions";

    public void initMQTTClient() {

        if(client == null) {
            try {
                String brokerHost = "chat.sendyit.com";
                String brokerPort ="8883";
                String clientId = "mother2";
                String user = "sendy";
                String password ="93a3a43dbac9ddd362702fb525b42a2d";
                if(brokerHost != null && brokerPort != null && clientId != null && user != null && password != null) {
                    String broker = "tcp://"+brokerHost+":"+brokerPort;
                    MemoryPersistence persistence = new MemoryPersistence();
                   // Log.i("Connecting to MQTT broker "+broker);
                    client = new MqttClient(broker, clientId, persistence);
                    MqttConnectOptions options = new MqttConnectOptions();
                    options.setCleanSession(true);
                    options.setUserName(user);
                    options.setPassword(password.toCharArray());
                    client.connect();
                    setMQTTCallback();
                    subscribeToTopics();
                }
            } catch (MqttException e) {
               // Log.e("An error occurred while trying to connect as the MQTT 'mother' client", e);
            }
        } else {
            if(!client.isConnected()) {
                try {
                    client.connect();
                    setMQTTCallback();
                    subscribeToTopics();
                } catch (MqttException e) {
                  //  Log.e("An error occurred while trying to connect the MQTTClient", e);
                }
            }
        }
    }

    private void subscribeToTopics() {
        if(client != null) {
            try {
                //subscribe to the location topics
                client.subscribe(TOPIC_PARTNER_APP_POSITIONS+"/#", 2);
                client.subscribe(TOPIC_CUSTOMER_POSITIONS+"/#", 2);
            } catch (MqttException e) {
                e.printStackTrace();
            }
        }
    }

    private void setMQTTCallback() {
        if(client != null) {
            client.setCallback(new MqttCallback() {
                public void connectionLost(Throwable throwable) {

                }

                public void messageArrived(String s, MqttMessage mqttMessage) throws Exception {
                    handleReceivedPush(s, mqttMessage);
                }

                public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

                }
            });
        }
    }

    private void handleReceivedPush(String topic, MqttMessage message) {
       // Log.d("Received a message from topic "+topic);
        if(topic != null) {
            if(topic.startsWith(TOPIC_PARTNER_APP_POSITIONS)) {
                if(message != null && message.getPayload() != null) {
                    System.out.println("MQTT MESSAGE >>> "+message.toString());
                    PartnerPositions.savePartnerPositionRedis(message.toString());

                }
            } else if(topic.startsWith(TOPIC_CUSTOMER_POSITIONS)) {
                if(message != null && message.getPayload() != null) {

                }
            }
        } else {
          //  Log.w("The received MQTT message has a null topic");
        }
    }
}
