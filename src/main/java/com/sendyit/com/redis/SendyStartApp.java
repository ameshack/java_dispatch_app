package com.sendyit.com.redis;

import com.lambdaworks.redis.RedisClient;
import com.lambdaworks.redis.api.StatefulRedisConnection;
import com.sendyit.com.mqttclient.SendyMqttClient;
import com.sendyit.com.rabbitmq.DispatchConsumer;
import com.sendyit.com.rabbitmq.RabbitMqConnections;
import com.sendyit.com.sendyutils.databases;

/**
 * Created by alloysmeshack on 17/06/2016.
 */
public class SendyStartApp {

    public static void main(String[] args)
    {
        startApp();
    }

    /** This method starts the dispatch app */
    public static void startApp()
    {
        databases.SENDY_REDIS_CLIENT= RedisClient.create("redis://localhost:6379/0");
        StatefulRedisConnection<String, String> connection =  databases.SENDY_REDIS_CLIENT.connect();
        databases.syncCommands = connection.async();

        /** start rabbit mq consumer connection */
        RabbitMqConnections.consumerConnection();
        /** start rabbit mq producer connection */
        RabbitMqConnections.producerConnection();
        /** Start rabbit mq consumer */
        DispatchConsumer.sendyDispatchConsume();
        /** Start mqtt client*/
      //  new SendyMqttClient().initMQTTClient();

    }
}
