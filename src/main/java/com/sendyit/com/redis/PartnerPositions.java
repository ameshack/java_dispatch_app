package com.sendyit.com.redis;

import com.lambdaworks.redis.RedisClient;
import com.lambdaworks.redis.api.StatefulRedisConnection;
import com.lambdaworks.redis.api.async.RedisAsyncCommands;
import com.sendyit.com.sendyutils.databases;
import org.json.JSONObject;

/**
 * Created by alloysmeshack on 17/06/2016.
 */
public class PartnerPositions {


    /**
     * This method saves partner positions received from MQTT
     * @param messagePayload
     */
    public static void savePartnerPositionRedis(String messagePayload)
    {


        try {
            JSONObject messageObject=new JSONObject(messagePayload);

            //TODO: do this in a future queue
            /** Only write active idle partners */
            if(messageObject.getBoolean("active"))
            {

                databases.syncCommands.geoadd("partner_idle_location:"+messageObject.getInt("vendor_type"),
                        messageObject.getDouble("lng"),
                        messageObject.getDouble("lat"),"partner:"+messageObject.getInt("rider_id"));
            }

            /** write all partners */
            databases.syncCommands.geoadd("partner_location:"+messageObject.getInt("vendor_type"),
                    messageObject.getDouble("lng"),
                    messageObject.getDouble("lat"),"partner:"+messageObject.getInt("rider_id"));


        }
     catch (Exception e) {
        e.printStackTrace();
    }


    }
}
