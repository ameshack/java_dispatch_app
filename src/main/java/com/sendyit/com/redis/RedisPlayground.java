package com.sendyit.com.redis;
import com.lambdaworks.redis.GeoArgs;
import com.lambdaworks.redis.GeoWithin;
import com.lambdaworks.redis.RedisClient;
import com.lambdaworks.redis.RedisFuture;
import com.lambdaworks.redis.api.StatefulRedisConnection;
import com.lambdaworks.redis.api.async.RedisAsyncCommands;
import com.sendyit.com.rabbitmq.DispatchConsumer;
import com.sendyit.com.rabbitmq.DispatchProducer;
import com.sendyit.com.rabbitmq.RabbitMqConnections;
import com.sendyit.com.sendyutils.databases;
import org.json.JSONArray;
import org.json.JSONObject;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Created by alloysmeshack on 11/06/2016.
 */
public class RedisPlayground {

    public static void main(String[] args)
    {
        testRedis();
    }


    public static void testRedis(){
        RedisClient redisClient = RedisClient.create("redis://localhost:6379/0");
        StatefulRedisConnection<String, String> connection = redisClient.connect();
        RedisAsyncCommands<String, String> syncCommands = connection.async();

       // RedisFuture<Long> addValue=commands.geoadd("positionsmine",44.968046,-94.420307,"rider:1");

        /*syncCommands.geoadd("holdlocation",36.859756,-1.217277,"rider:1");
        syncCommands.geoadd("holdlocation",36.863769, -1.217245,"rider:2"); //0.4462
        syncCommands.geoadd("holdlocation",36.846570, -1.217792,"rider:3"); //1.4674
        syncCommands.geoadd("holdlocation",36.851838,-1.215722,"rider:4");*/ //0.8973

       // RabbitMqConnections.consumerConnection();
        //DispatchConsumer.sendyDispatchConsume();

       // RabbitMqConnections.producerConnection();
        //DispatchProducer.sendyDispatchProducer();

        Calendar calendar = Calendar.getInstance();
        Date now = calendar.getTime();
        Timestamp currentTimestamp = new java.sql.Timestamp(now.getTime());
        System.out.println("time >> "+now.getTime());




        try {

            GeoArgs rankingArguments=new GeoArgs();
            rankingArguments.withDistance().asc();
            rankingArguments.withCoordinates();

            //RedisFuture<String> future = databases.syncCommands.get("holdlocation");
            //RedisFuture<Double> distanceBtwn=databases.syncCommands.geodist("holdlocation","rider:1","rider:4", GeoArgs.Unit.km);
            RedisFuture<List<GeoWithin<String>>> listLocations=syncCommands.georadius("partner_idle_location:9",36.859756,-1.217277,16.5,
                    GeoArgs.Unit.km,rankingArguments);

           // String value = future.get(1, TimeUnit.MINUTES);
           // System.out.println(distanceBtwn.get());
            System.out.println(listLocations.get());
            JSONArray partnerArray=new JSONArray();
            List<GeoWithin<String>> list =  listLocations.get();
            for(int i=0; i<list.size(); i++) {
                System.out.println("Stored string in redis:: "+list.get(i));
                System.out.println("Member :: "+list.get(i).member);
                System.out.println("Distance :: "+list.get(i).distance);

                JSONObject partnerObject=new JSONObject();
                partnerObject.put("rider_id",Integer.parseInt(list.get(i).member.split(":")[1]));
                partnerObject.put("distance",list.get(i).distance.doubleValue());
                partnerObject.put("long",list.get(i).coordinates.x.doubleValue());
                partnerObject.put("lat",list.get(i).coordinates.y.doubleValue());
                partnerObject.put("vendor_type",9);
                partnerObject.put("rank",i);

                partnerArray.put(partnerObject);
            }
            JSONObject partnerList=new JSONObject();
            partnerList.put("partner_list",partnerArray);
            System.out.println("JSON :: "+partnerList.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }


        connection.close();
       // redisClient.shutdown();
    }
}
