package com.sendyit.com.rabbitmq;
import com.rabbitmq.client.*;
import com.sendyit.com.dispatchapi.PartnerList;
import com.sendyit.com.sendyutils.ValidationClass;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * Created by alloysmeshack on 19/06/2016.
 */
public class DispatchConsumer {
    public static final String TASK_QUEUE_NAME = "partner_dispatch_queue";
    private static Channel CONSUMER_channel;


    public static void sendyDispatchConsume()
    {
        try {

            CONSUMER_channel = RabbitMqConnections.CONSUMER_connection.createChannel();
            CONSUMER_channel.queueDeclare(TASK_QUEUE_NAME, true, false, false, null);
            CONSUMER_channel.basicQos(1);

            final Consumer consumer = new DefaultConsumer(CONSUMER_channel) {
                @Override
                public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                    String message = new String(body, "UTF-8");

                    System.out.println(" [x] Received '" + message + "'");
                    try {
                        /** process message here */
                        if(ValidationClass.isJSONValid(message))
                        {
                            JSONObject payloadObject=new JSONObject(message);
                            /** dispatch partner list */
                        if(payloadObject.getString("message_type").equalsIgnoreCase(MessageTypes.DISPATCH_PARTNER_LIST))
                        {
                            PartnerList.eligiblePartnerList(16,9,-1.217277,36.859756); //TODO: call this in an actor
                        }

                        }

                    } finally {
                        System.out.println(" [x] Done");
                        CONSUMER_channel.basicAck(envelope.getDeliveryTag(), false);
                    }
                }
            };
            CONSUMER_channel.basicConsume(TASK_QUEUE_NAME, false, consumer);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
