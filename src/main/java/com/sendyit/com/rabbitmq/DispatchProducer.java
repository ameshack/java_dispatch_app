package com.sendyit.com.rabbitmq;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * Created by alloysmeshack on 19/06/2016.
 */
public class DispatchProducer {
    public static final String TASK_QUEUE_NAME = "dispatch_list_queue";
    private static Channel PRODUCER_channel;

    public static void sendyDispatchProducer(String message)
    {
        try {


            PRODUCER_channel = RabbitMqConnections.CONSUMER_connection.createChannel();
            PRODUCER_channel.queueDeclare(TASK_QUEUE_NAME, true, false, false, null);

            PRODUCER_channel.basicPublish( "", TASK_QUEUE_NAME,
                    MessageProperties.PERSISTENT_TEXT_PLAIN,
                    message.getBytes());
            System.out.println(" [x] Sent '" + message + "'");
        }
     catch (IOException e) {
        e.printStackTrace();
    }

    }

    public static void main(String[] argv){
        try {
            String messageTest="Hey how are you doing";

            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost("localhost");
            factory.setAutomaticRecoveryEnabled(true);
            Connection connection= factory.newConnection();

            Channel PRODUCER_channel = connection.createChannel();
            PRODUCER_channel.queueDeclare(TASK_QUEUE_NAME, true, false, false, null);
            String message = getMessage(argv);

            PRODUCER_channel.basicPublish( "", TASK_QUEUE_NAME,
                    MessageProperties.PERSISTENT_TEXT_PLAIN,
                    messageTest.getBytes());
            System.out.println(" [x] Sent '" + messageTest + "'");
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        catch (TimeoutException e) {
            e.printStackTrace();
        }
    }

    private static String getMessage(String[] strings){
        if (strings.length < 1)
            return "Hello World!";
        return joinStrings(strings, " ");
    }

    private static String joinStrings(String[] strings, String delimiter) {
        int length = strings.length;
        if (length == 0) return "";
        StringBuilder words = new StringBuilder(strings[0]);
        for (int i = 1; i < length; i++) {
            words.append(delimiter).append(strings[i]);
        }
        return words.toString();
    }
}
