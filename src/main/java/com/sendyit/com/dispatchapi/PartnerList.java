package com.sendyit.com.dispatchapi;

import com.lambdaworks.redis.GeoArgs;
import com.lambdaworks.redis.GeoWithin;
import com.lambdaworks.redis.RedisFuture;
import com.sendyit.com.rabbitmq.DispatchProducer;
import com.sendyit.com.rabbitmq.MessageTypes;
import com.sendyit.com.sendyutils.databases;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by alloysmeshack on 18/06/2016.
 * this class creates a partner list based on location proximity
 */
public class PartnerList {

    /**
     * This method gets the list of partners within the specified vicinity of point of interest
     * @param maxRadius the maximum distance defining the vicinity
     * @param vendorType the vendor being looked for
     * @param orderLat the order latitute
     * @param orderLong the order longitude
     */
    public static void eligiblePartnerList(int maxRadius,int vendorType,double orderLat,double orderLong)
    {
        JSONObject partnerList=new JSONObject();
        try {

            GeoArgs rankingArguments=new GeoArgs();
            rankingArguments.withDistance().asc();
            rankingArguments.withCoordinates();

            RedisFuture<List<GeoWithin<String>>> listLocations= databases.syncCommands.georadius("partner_idle_location:"+vendorType,
                    orderLong,orderLat,maxRadius,
                    GeoArgs.Unit.km,rankingArguments);

            JSONArray partnerArray=new JSONArray();
            List<GeoWithin<String>> list =  listLocations.get();
            for(int i=0; i<list.size(); i++)
            {
                JSONObject partnerObject=new JSONObject();
                partnerObject.put("rider_id",Integer.parseInt(list.get(i).member.split(":")[1]));
                partnerObject.put("distance",list.get(i).distance.doubleValue());
                partnerObject.put("long",list.get(i).coordinates.x.doubleValue());
                partnerObject.put("lat",list.get(i).coordinates.y.doubleValue());
                partnerObject.put("vendor_type",vendorType);
                partnerObject.put("rank",i);

                partnerArray.put(partnerObject);

            }
            partnerList.put("message_type", MessageTypes.DISPATCH_PARTNER_LIST);
            partnerList.put("partner_list",partnerArray);


            /** Send message to Rabitt MQ with the partner list for the notification to be sent to partners */
            DispatchProducer.sendyDispatchProducer(partnerList.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
